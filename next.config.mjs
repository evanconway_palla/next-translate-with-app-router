import nextTranslate from "next-translate-plugin";
/** @type {import('next').NextConfig} */
const nextConfig = nextTranslate({});

export default nextConfig;
