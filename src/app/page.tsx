import useTranslation from "next-translate/useTranslation";
import Link from "next/link";
import i18nConfig from "../../i18n.json";

export default function Home() {
  const { t, lang } = useTranslation("common");
  const { locales } = i18nConfig;
  return (
    <div>
      <div>{lang}</div>
      <div>{t("title")}</div>
      <div>{t("variable-example", { count: 42 })}</div>
      {locales.map((lang) => (
        <div key={lang}>
          <Link href={`/?lang=${lang}`}>{lang}</Link>
        </div>
      ))}
    </div>
  );
}
