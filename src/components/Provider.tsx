import { ReactNode, createContext, useContext, useState } from "react";
import getNamespace from "../../localesJS";
import I18nProvider from "next-translate/I18nProvider";

const LanguageContext = createContext({
  locale: i18nConfig.defaultLocale,
  setLocale: (lang: string) => {},
});

export const useSetLanguage = () => {
  const langCtx = useContext(LanguageContext);
  return (lang: string) => langCtx.setLocale(lang);
};

const Provider = ({ children }: { children: ReactNode }) => {
  const [locale, setLocale] = useState(i18nConfig.defaultLocale);

  return (
    <LanguageContext.Provider value={{ locale, setLocale }}>
      <I18nProvider namespaces={getNamespace(locale)}>{children}</I18nProvider>
    </LanguageContext.Provider>
  );
};

export default Provider;
