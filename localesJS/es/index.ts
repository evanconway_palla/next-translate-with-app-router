import { copyToI18nDictionary } from "../copy";

const es = copyToI18nDictionary({
  title: "Hola Mundo",
  variableExample: "Usando una variable {{count}}",
});

export default es;
