import { I18nDictionary } from "next-translate";

interface Copy {
  title: string;
  variableExample: string;
}

export const copyToI18nDictionary = (copy: Copy) => {
  // This is gross. Find a more type-safe way later.
  return copy as unknown as Record<string, I18nDictionary>;
};
