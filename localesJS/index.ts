import en from "./en";
import es from "./es";
import fr from "./fr";

/*
A function to get namespaces given a locale. We do this 
instead of just exporting an object to help with type
safety.
*/

const getNamespace = (locale: string | undefined) => {
  if (locale === "en") return en;
  if (locale === "es") return es;
  if (locale === "fr") return fr;
  return en;
};

export default getNamespace;
