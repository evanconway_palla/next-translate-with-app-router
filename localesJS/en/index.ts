import { copyToI18nDictionary } from "../copy";

const en = copyToI18nDictionary({
  title: "Hello World",
  variableExample: "Using a variable {{count}}",
});

export default en;
