import { copyToI18nDictionary } from "../copy";

const fr = copyToI18nDictionary({
  title: "Bonjour le monde",
  variableExample: "Utiliser une variable {{count}}",
});

export default fr;
